import cv2

niz_lokacija = ['.\Slike\Arsene\Arsene', '.\Slike\Drogba\didier', '.\Slike\Jose\jose']
niz_imena = ['\Arsene', '\didier', '\jose']

for i in range(3):
    for j in range(1, 11):
        img = cv2.imread(niz_lokacija[i] + str(j) + '.jpg')
        if img is not None:
            if j <= 7:
                cv2.imwrite('.\TRAIN' + niz_imena[i] + str(j) + '.jpg', img)
            else:
                cv2.imwrite('.\TEST' + niz_imena[i] + str(j) + '.jpg', img)