import numpy as np
import cv2
from PIL import Image, ImageEnhance 

def poboljsanjeOsvijetljenja(niz_ROI, niz_lokacija_Osvjetljenje):
    for i in range(len(niz_ROI)):
        img = Image.open(niz_ROI[i])
        enhancer = ImageEnhance.Brightness(img)
        enhanced_img = enhancer.enhance(1.8)
        enhanced_img.save(niz_lokacija_Osvjetljenje[i])

def poboljsanjeKontrasta(niz_ROI, niz_lokacija_Kontrast):
    for i in range(len(niz_ROI)):
        img = Image.open(niz_ROI[i])
        enhancer = ImageEnhance.Contrast(img)
        enhanced_img = enhancer.enhance(1.8)
        enhanced_img.save(niz_lokacija_Kontrast[i])

def poboljsanjeHistogram(niz_ROI, niz_lokacija_Histogram):
    for i in range(len(niz_ROI)):
        img = cv2.imread(niz_ROI[i], 0)
        img_item = cv2.equalizeHist(img)
        cv2.imwrite(niz_lokacija_Histogram[i], img_item)



niz_ROI = [".\Lokacije\ROI-Didier1.bmp", '.\Lokacije\ROI-Didier2.bmp', '.\Lokacije\ROI-Didier3.bmp', '.\Lokacije\ROI-Didier4.bmp', '.\Lokacije\ROI-Didier5.bmp', '.\Lokacije\ROI-Arsene1.bmp', '.\Lokacije\ROI-Arsene2.bmp', '.\Lokacije\ROI-Arsene3.bmp', '.\Lokacije\ROI-Arsene4.bmp', '.\Lokacije\ROI-Arsene5.bmp', '.\Lokacije\ROI-Jose1.bmp', '.\Lokacije\ROI-Jose2.bmp', '.\Lokacije\ROI-Jose3.bmp', '.\Lokacije\ROI-Jose4.bmp', '.\Lokacije\ROI-Jose5.bmp']
niz_lokacija_Osvjetljenje = ['.\LokacijePKSO\PKSO-Didier1.bmp', '.\LokacijePKSO\PKSO-Didier2.bmp', '.\LokacijePKSO\PKSO-Didier3.bmp', '.\LokacijePKSO\PKSO-Didier4.bmp', '.\LokacijePKSO\PKSO-Didier5.bmp', '.\LokacijePKSO\PKSO-Arsene1.bmp', '.\LokacijePKSO\PKSO-Arsene2.bmp', '.\LokacijePKSO\PKSO-Arsene3.bmp', '.\LokacijePKSO\PKSO-Arsene4.bmp', '.\LokacijePKSO\PKSO-Arsene5.bmp', '.\LokacijePKSO\PKSO-Jose1.bmp', '.\LokacijePKSO\PKSO-Jose2.bmp', '.\LokacijePKSO\PKSO-Jose3.bmp', '.\LokacijePKSO\PKSO-Jose4.bmp', '.\LokacijePKSO\PKSO-Jose5.bmp']
niz_lokacija_Kontrast = ['.\LokacijePKSK\PKSK-Didier1.bmp', '.\LokacijePKSK\PKSK-Didier2.bmp', '.\LokacijePKSK\PKSK-Didier3.bmp', '.\LokacijePKSK\PKSK-Didier4.bmp', '.\LokacijePKSK\PKSK-Didier5.bmp', '.\LokacijePKSK\PKSK-Arsene1.bmp', '.\LokacijePKSK\PKSK-Arsene2.bmp', '.\LokacijePKSK\PKSK-Arsene3.bmp', '.\LokacijePKSK\PKSK-Arsene4.bmp', '.\LokacijePKSK\PKSK-Arsene5.bmp', '.\LokacijePKSK\PKSK-Jose1.bmp', '.\LokacijePKSK\PKSK-Jose2.bmp', '.\LokacijePKSK\PKSK-Jose3.bmp', '.\LokacijePKSK\PKSK-Jose4.bmp', '.\LokacijePKSK\PKSK-Jose5.bmp']
niz_lokacija_Histogram = ['.\LokacijePKSH\PKSH-Didier1.bmp', '.\LokacijePKSH\PKSH-Didier2.bmp', '.\LokacijePKSH\PKSH-Didier3.bmp', '.\LokacijePKSH\PKSH-Didier4.bmp', '.\LokacijePKSH\PKSH-Didier5.bmp', '.\LokacijePKSH\PKSH-Arsene1.bmp', '.\LokacijePKSH\PKSH-Arsene2.bmp', '.\LokacijePKSH\PKSH-Arsene3.bmp', '.\LokacijePKSH\PKSH-Arsene4.bmp', '.\LokacijePKSH\PKSH-Arsene5.bmp', '.\LokacijePKSH\PKSH-Jose1.bmp', '.\LokacijePKSH\PKSH-Jose2.bmp', '.\LokacijePKSH\PKSH-Jose3.bmp', '.\LokacijePKSH\PKSH-Jose4.bmp', '.\LokacijePKSH\PKSH-Jose5.bmp']

poboljsanjeOsvijetljenja(niz_ROI, niz_lokacija_Osvjetljenje)
poboljsanjeKontrasta(niz_ROI, niz_lokacija_Kontrast)
poboljsanjeHistogram(niz_ROI, niz_lokacija_Histogram)