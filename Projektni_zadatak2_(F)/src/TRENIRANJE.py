import os
import cv2
import numpy as np
from PIL import Image, ImageEnhance 
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
slike_dir = os.path.join(BASE_DIR, "TRAIN")

face_cascade = cv2.CascadeClassifier('cascades\data\haarcascade_frontalface_alt.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()

trenutni_id = 0
oznake_id_dict = {}
oznake = []
x_train = []

for korjen, direktoriji, fajlovi in os.walk(slike_dir):
    for file in fajlovi:
        if file.endswith("png") or file.endswith("jpg"):
            put = os.path.join(korjen, file)
            if file.startswith("Arsene"):
                oznaka = "Arsene Wenger"
            elif file.startswith("didier"):
                oznaka = "Didier Drogba"
            elif file.startswith("jose"):
                oznaka = "Jose Mourinho"
            else:
                oznaka = "Unknown"
            if not oznaka in oznake_id_dict:
                oznake_id_dict[oznaka] = trenutni_id
                trenutni_id += 1
            id_ = oznake_id_dict[oznaka]
            
            slika_pil = Image.open(put).convert("L") # Grayscale
            #Poboljsanje slike
            enhancer = ImageEnhance.Contrast(slika_pil)
            enhanced_img = enhancer.enhance(1.8)
            niz_slika = np.array(enhanced_img, "uint8") # Pretvaranje slike u broj tj. niz brojeva
            faces = face_cascade.detectMultiScale(niz_slika, scaleFactor=1.5, minNeighbors=2)
            for (x, y, w, h) in faces:
                roi = niz_slika[y:y+h, x:x+w]
                x_train.append(roi)
                oznake.append(id_)


with open("oznake.pickle", 'wb') as fajl:
    pickle.dump(oznake_id_dict, fajl)
 
recognizer.train(x_train, np.array(oznake))
recognizer.save("trainner.yml") # YAML Ain't Markup Language
