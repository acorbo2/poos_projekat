import numpy as np
import cv2

face_cascade = cv2.CascadeClassifier('cascades\data\haarcascade_frontalface_alt.xml')

niz_slika = ['.\Slike\Drogba\didier1.jpg', '.\Slike\Drogba\didier2.jpg', '.\Slike\Drogba\didier3.jpg', '.\Slike\Drogba\didier4.jpg', '.\Slike\Drogba\didier5.jpg', '.\Slike\Arsene\Arsene2.jpg', '.\Slike\Arsene\Arsene9.jpg', '.\Slike\Arsene\Arsene3.jpg', '.\Slike\Arsene\Arsene4.jpg', '.\Slike\Arsene\Arsene5.jpg', '.\Slike\Jose\jose1.jpg', '.\Slike\Jose\jose2.jpg', '.\Slike\Jose\jose3.jpg', '.\Slike\Jose\jose4.jpg', '.\Slike\Jose\jose5.jpg']
niz_lokacija = ['.\Lokacije\ROI-Didier1.bmp', '.\Lokacije\ROI-Didier2.bmp', '.\Lokacije\ROI-Didier3.bmp', '.\Lokacije\ROI-Didier4.bmp', '.\Lokacije\ROI-Didier5.bmp', '.\Lokacije\ROI-Arsene1.bmp', '.\Lokacije\ROI-Arsene2.bmp', '.\Lokacije\ROI-Arsene3.bmp', '.\Lokacije\ROI-Arsene4.bmp', '.\Lokacije\ROI-Arsene5.bmp', '.\Lokacije\ROI-Jose1.bmp', '.\Lokacije\ROI-Jose2.bmp', '.\Lokacije\ROI-Jose3.bmp', '.\Lokacije\ROI-Jose4.bmp', '.\Lokacije\ROI-Jose5.bmp']

for i in range(len(niz_slika)):
    cap = cv2.imread(niz_slika[i])
    gray  = cv2.cvtColor(cap, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(cap, scaleFactor=1.5, minNeighbors=2)
    print(faces)
    for (x, y, w, h) in faces:
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = cap[y:y+h, x:x+w]
        img_item = niz_lokacija[i]
        cv2.imwrite(img_item, roi_color) #ili roi_gray, zavisno sta nam treba