import numpy as np
import os
from PIL import Image
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pickle
from sklearn.metrics import confusion_matrix

face_cascade = cv2.CascadeClassifier('cascades\data\haarcascade_frontalface_alt.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainner.yml")

oznake = {}
with open("oznake.pickle", 'rb') as fajl:
    original_oznake = pickle.load(fajl)
    oznake = {vrijednost:kljuc for kljuc, vrijednost in original_oznake.items()}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
testne_slike_dir = os.path.join(BASE_DIR, "TEST")
lokacije_prepoznavanje_dir = os.path.join(BASE_DIR, "LokacijePREPOZNAVANJE")
lokacije_dir = 'C:\\Users\\Azer\\Desktop\\poos_projekat_II\\Projektni_zadatak2_(F)\\src\\LokacijePREPOZNAVANJE\\'

def testiranjeModela():
    tacneOznake = []
    predvidjeneOznake = []
    brojac = 0
    for korjen, direktoriji, fajlovi in os.walk(testne_slike_dir):
        for file in fajlovi:
            if file.endswith("png") or file.endswith("jpg"):
                put = os.path.join(korjen, file)
                tacneOznake.append(vratiOznaku(os.path.splitext(file)[0]))
                cap = cv2.imread(str(put))
                gray  = cv2.cvtColor(cap, cv2.COLOR_BGR2GRAY)
                faces = face_cascade.detectMultiScale(cap, scaleFactor=1.5, minNeighbors=2)
                for (x, y, w, h) in faces:
                    roi_gray = gray[y:y+h, x:x+w]
                    roi_color = cap[y:y+h, x:x+w]

                    id_, confidence = recognizer.predict(roi_gray)
                    if confidence >= 30:
                        img_item = lokacije_dir + oznake[id_] + str(brojac) + '.bmp'
                        predvidjeneOznake.append(oznake[id_])
                        cv2.rectangle(cap, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        font = cv2.FONT_HERSHEY_SIMPLEX
                        name = oznake[id_]
                        color = (255, 255, 255)
                        stroke = 2
                        cv2.putText(cap, name, (x, y), font, 1, color, stroke, cv2.LINE_AA)
                        cv2.imwrite(img_item, cap) #ili roi_gray, zavisno sta nam treba
                        brojac += 1
                        break  
    return tacneOznake, predvidjeneOznake

def vratiOznaku(oznaka):
    if oznaka.startswith("Arsene"):
        oznaka = "Arsene Wenger"
    elif oznaka.startswith("didier"):
        oznaka = "Didier Drogba"
    elif oznaka.startswith("jose"):
        oznaka = "Jose Mourinho"
    else:
        oznaka = "Unknown"
    return oznaka

def accuracy(indeks, mat):
    TP = mat[indeks][indeks]
    TN = 0
    ALL = 0
    for i in range(len(mat[0])):
        for j in range(len(mat[0])):
            ALL += mat[i][j]
            if i == indeks or j == indeks:
                continue
            TN += mat[i][j]
    return (TP+TN)/ALL

def sensitivity(indeks, mat):
    TP = mat[indeks][indeks]
    FN = 0
    for i in range(len(mat[0])):
        if i != indeks:
            FN += mat[indeks][i]
    return TP/(TP+FN)

def specificity(indeks, mat):
    TN = 0
    FP = 0
    for i in range(len(mat[0])):
        if i == indeks:
            continue
        for j in range(len(mat[0])):
            if j == indeks:
                continue
            TN += mat[i][j]
        FP += mat[i][indeks]
    return TN/(TN+FP)

tacneOznake, predvidjeneOznake = testiranjeModela()
confusion_matrica = confusion_matrix(tacneOznake, predvidjeneOznake, labels=["Arsene Wenger", "Didier Drogba", "Jose Mourinho"])

#Racunanje performansi
for i in range(len(confusion_matrica)):
    print("Klasa " + oznake[i] + ": ", end=" ")
    print("sensitivity: " + str(sensitivity(i, confusion_matrica)) + ", specificity: " + str(specificity(i, confusion_matrica)) + ", accuracy: " + str(accuracy(i, confusion_matrica)))