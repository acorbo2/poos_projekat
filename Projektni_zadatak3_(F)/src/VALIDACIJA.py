import numpy as np
import os
from PIL import Image, ImageEnhance 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import pickle

face_cascade = cv2.CascadeClassifier('cascades\data\haarcascade_frontalface_alt.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainner.yml")

oznake = {}
with open("oznake.pickle", 'rb') as fajl:
    original_oznake = pickle.load(fajl)
    oznake = {vrijednost:kljuc for kljuc, vrijednost in original_oznake.items()}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
lokacije_dir = 'C:\\Users\\Azer\\Desktop\\POOS\\Projektni_zadatak3_(F)\\src\\LokacijeVALIDACIJA\\'

def main(detekcija_slike_putanja):
    brojac = 0
    for korjen, direktoriji, fajlovi in os.walk(detekcija_slike_putanja):
        for file in fajlovi:
            if file.endswith("png") or file.endswith("jpg"):
                put = os.path.join(korjen, file)
              
                cap = Image.open(put).convert("L")
                enhancer = ImageEnhance.Contrast(cap)
                enhanced_img = enhancer.enhance(1.8)
               
                enhanced_img = np.array(enhanced_img)

                faces = face_cascade.detectMultiScale(enhanced_img, scaleFactor=1.5, minNeighbors=2)
                for (x, y, w, h) in faces:
                    roi_gray = np.array(cap)[y:y+h, x:x+w]
                    roi_color = enhanced_img[y:y+h, x:x+w]

                    id_, confidence = recognizer.predict(roi_gray)
                    if confidence >= 30:
                        img_item = lokacije_dir + oznake[id_] + str(brojac) + '.bmp'
                        cv2.rectangle(enhanced_img, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        font = cv2.FONT_HERSHEY_SIMPLEX
                        name = oznake[id_]
                        color = (255, 255, 255)
                        stroke = 2
                        cv2.putText(enhanced_img, name, (x, y), font, 1, color, stroke, cv2.LINE_AA)
                        cv2.imwrite(img_item, enhanced_img) #ili roi_gray, zavisno sta nam treba
                        brojac += 1
                        break  

validacija_slike_dir = os.path.join(BASE_DIR, "SlikeZaValidaciju")
main(validacija_slike_dir)
