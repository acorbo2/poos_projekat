import numpy as np
import cv2

niz_ROI = ['.\Lokacije\ROI-Didier1.bmp', '.\Lokacije\ROI-Didier2.bmp', '.\Lokacije\ROI-Didier3.bmp', '.\Lokacije\ROI-Didier4.bmp', '.\Lokacije\ROI-Didier5.bmp', '.\Lokacije\ROI-Arsene1.bmp', '.\Lokacije\ROI-Arsene2.bmp', '.\Lokacije\ROI-Arsene3.bmp', '.\Lokacije\ROI-Arsene4.bmp', '.\Lokacije\ROI-Arsene5.bmp', '.\Lokacije\ROI-Jose1.bmp', '.\Lokacije\ROI-Jose2.bmp', '.\Lokacije\ROI-Jose3.bmp', '.\Lokacije\ROI-Jose4.bmp', '.\Lokacije\ROI-Jose5.bmp']
niz_lokacija = ['.\LokacijeSUM\SUM-Didier1.bmp', '.\LokacijeSUM\SUM-Didier2.bmp', '.\LokacijeSUM\SUM-Didier3.bmp', '.\LokacijeSUM\SUM-Didier4.bmp', '.\LokacijeSUM\SUM-Didier5.bmp', '.\LokacijeSUM\SUM-Arsene1.bmp', '.\LokacijeSUM\SUM-Arsene2.bmp', '.\LokacijeSUM\SUM-Arsene3.bmp', '.\LokacijeSUM\SUM-Arsene4.bmp', '.\LokacijeSUM\SUM-Arsene5.bmp', '.\LokacijeSUM\SUM-Jose1.bmp', '.\LokacijeSUM\SUM-Jose2.bmp', '.\LokacijeSUM\SUM-Jose3.bmp', '.\LokacijeSUM\SUM-Jose4.bmp', '.\LokacijeSUM\SUM-Jose5.bmp']

for i in range(len(niz_ROI)):
    img = cv2.imread(niz_ROI[i])
    #img_item = cv2.GaussianBlur(img, (5, 5), 1)
    img_item = cv2.medianBlur(img, 3)
    cv2.imwrite(niz_lokacija[i], img_item)
