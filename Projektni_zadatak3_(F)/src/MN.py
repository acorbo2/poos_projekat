import numpy as np
import cv2

niz_ROI = ['.\Lokacije\ROI-Didier1.bmp', '.\Lokacije\ROI-Didier2.bmp', '.\Lokacije\ROI-Didier3.bmp', '.\Lokacije\ROI-Didier4.bmp', '.\Lokacije\ROI-Didier5.bmp', '.\Lokacije\ROI-Arsene1.bmp', '.\Lokacije\ROI-Arsene2.bmp', '.\Lokacije\ROI-Arsene3.bmp', '.\Lokacije\ROI-Arsene4.bmp', '.\Lokacije\ROI-Arsene5.bmp', '.\Lokacije\ROI-Jose1.bmp', '.\Lokacije\ROI-Jose2.bmp', '.\Lokacije\ROI-Jose3.bmp', '.\Lokacije\ROI-Jose4.bmp', '.\Lokacije\ROI-Jose5.bmp']
niz_lokacija = ['.\LokacijeMN\MN-Didier1.bmp', '.\LokacijeMN\MN-Didier2.bmp', '.\LokacijeMN\MN-Didier3.bmp', '.\LokacijeMN\MN-Didier4.bmp', '.\LokacijeMN\MN-Didier5.bmp', '.\LokacijeMN\MN-Arsene1.bmp', '.\LokacijeMN\MN-Arsene2.bmp', '.\LokacijeMN\MN-Arsene3.bmp', '.\LokacijeMN\MN-Arsene4.bmp', '.\LokacijeMN\MN-Arsene5.bmp', '.\LokacijeMN\MN-Jose1.bmp', '.\LokacijeMN\MN-Jose2.bmp', '.\LokacijeMN\MN-Jose3.bmp', '.\LokacijeMN\MN-Jose4.bmp', '.\LokacijeMN\MN-Jose5.bmp']

for i in range(len(niz_ROI)):
    img = cv2.imread(niz_ROI[i])
    imgBlur = cv2.blur(img, (2,2))
    imgMask = img - imgBlur
    img_item = img + imgMask
    cv2.imwrite(niz_lokacija[i], img_item)